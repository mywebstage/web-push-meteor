import { Template } from 'meteor/templating';
import { Mongo } from 'meteor/mongo';

const Push = new Mongo.Collection('push');

import './main.html';

Template.subscribe.onCreated(function helloOnCreated() {
  navigator.serviceWorker.register('service-worker.js');

  navigator.serviceWorker.ready
  .then(function(registration) {
    console.log('Service-Worker registered');
    return registration.pushManager.getSubscription();
  }).then(function(subscription) {
    if (subscription) {
      console.log('Already subscribed to Push');
    }
  });
});

Template.subscribe.helpers({
  subscribed(){
    var names = [];
    Push.find().forEach(function(p){
      names.push(p.name);
    });
    return names;
  }
});

function urlBase64ToUint8Array(base64String) {
    var padding = '='.repeat((4 - base64String.length % 4) % 4);
    var base64 = (base64String + padding)
        .replace(/\-/g, '+')
        .replace(/_/g, '/');

    var rawData = window.atob(base64);
    var outputArray = new Uint8Array(rawData.length);

    for (var i = 0; i < rawData.length; ++i) {
        outputArray[i] = rawData.charCodeAt(i);
    }
    return outputArray;
}

var my_sub = {};

Template.subscribe.events({
  'click #sub'(event, instance) {

    var name = document.getElementById("subname").value;

    if(name){
      navigator.serviceWorker.ready
      .then(async function(registration) {

        Meteor.call('push.vapidPublicKey',function(err,key){

          const convertedVapidKey = urlBase64ToUint8Array(key);

          registration.pushManager.subscribe({
            userVisibleOnly: true,
            applicationServerKey: convertedVapidKey
          }).then(function(subscription){

            Meteor.call("push.register",name,JSON.stringify(subscription));
            console.log('Subscribed: '+name);
          });

        });

      });
    }
  },
  'click #unsub'(){
    navigator.serviceWorker.ready
    .then(function(registration) {
      return registration.pushManager.getSubscription();
    }).then(function(subscription) {
      return subscription.unsubscribe()
        .then(function() {
          Meteor.call("push.unregister",JSON.stringify(subscription),function(err,res){
            console.log('Unsubscribed: '+ res);
          });
        });
    });
  },
  'click #send'(){
    var name = document.getElementById("sendname").value;
    var message = document.getElementById("sendmessage").value;
    console.log(message+" to "+name);
    Meteor.call("push.send",name,message);
  }
});
