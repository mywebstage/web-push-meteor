import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';

const Push = new Mongo.Collection('push');

const webPush = require('web-push');

const VAPID_PUBLIC_KEY = 'BDITip6m6NlzNv_NWgBHJXfgpUVf6ICe43ifI65OwsdFPSlWGIZjbUKizZQjxRPW9M78TafkP1-ZQxdUBXq6SWk';
const VAPID_PRIVATE_KEY = 'ntm6cHF50cjgkQVkISVx_ryLiiXSQWuBZ6iFCQkenvo';

if (!VAPID_PUBLIC_KEY || !VAPID_PRIVATE_KEY) {
  console.log("You must set the VAPID_PUBLIC_KEY and VAPID_PRIVATE_KEY "+
              "environment variables. You can use the following ones:");
  console.log(webPush.generateVAPIDKeys());
  return;
}else{
  webPush.setVapidDetails(
    'mailto:email@mywebstage.de',
    VAPID_PUBLIC_KEY,
    VAPID_PRIVATE_KEY
  );
}


Meteor.startup(() => {
  // code to run on server at startup
});

Meteor.methods({
  'push.register'(name,subscription) {
    console.log("Register: "+name);

    var data = JSON.parse(subscription);

    var subscriber = {
      name: name,
      endpoint: data.endpoint,
      keys: data.keys
    };

    var user = Push.findOne({endpoint: data.endpoint});

    if(user){
      Push.update(user._id,{
        name: name,
        endpoint: data.endpoint,
        keys: data.keys
       });
    }else{
      Push.insert(subscriber);
    }


  },
  'push.unregister'(subscription) {
    var subscriber = Push.findOne({endpoint: JSON.parse(subscription).endpoint});
    console.log("Unregister: "+subscriber.name);
    Push.remove(subscriber._id);
    return subscriber.name;
  },
  'push.vapidPublicKey'(){
    return VAPID_PUBLIC_KEY;
  },
  'push.send'(name,message){

    Push.find().forEach(function(subscriber){

      if(subscriber.name.indexOf(name) != -1){

        var subscription = {
          endpoint: subscriber.endpoint,
          keys: subscriber.keys
        };

        var payload = message;

        var options = {};

        webPush.sendNotification(
          subscription,
          payload,
          options
        ).then(function() {
          console.log('Push Application Server - Notification sent to ' + subscriber.name);
        }).catch(function(err) {
          console.log('ERROR in sending Notification ' + subscriber.name);
          console.log(err);
          Push.remove(subscriber._id);
        });
      }
    });
  }
});
