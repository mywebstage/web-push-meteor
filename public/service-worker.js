self.addEventListener('push', function(event) {
  const payload = event.data ? event.data.text() : '';
  event.waitUntil(self.registration.showNotification("Push Titel", {
    body: payload,
    icon: 'push.png',
  }));
});
